def sumar(a, b):
    return a + b

def restar(a, b):
    return a - b

if __name__ == '__main__':
    print(sumar(1, 2))
    print(sumar(3, 4))
    print(restar(6, 5))
    print(restar(8, 7))
